#
# This file is the ecam-init recipe.
#

SUMMARY = "Simple ecam-init application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://test_ecam.sh \
	   file://stream.sh \
	"

S = "${WORKDIR}"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

do_install() {
	     install -d ${D}/home/
	     install -d ${D}/home/root
	     install -m 0755 ${S}/test_ecam.sh ${D}/home/root
	     install -m 0755 ${S}/stream.sh ${D}/home/root
}
FILES_${PN} += "/home/root/*"
