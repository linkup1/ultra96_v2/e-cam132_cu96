DESCRIPTION = "PETALINUX image definition for Xilinx boards"
LICENSE = "MIT"

require recipes-core/images/petalinux-image-common.inc 

inherit extrausers 
COMMON_FEATURES = "\
		ssh-server-dropbear \
		hwcodecs \
		package-management \
		"
IMAGE_LINGUAS = " "

IMAGE_INSTALL = "\
		kernel-modules \
		bc \
		mtd-utils \
		usbutils \
		canutils \
		ethtool \
		openssh-sftp-server \
		git \
		pciutils \
		run-postinsts \
		libevdev \
		opencv \
		coreutils \
		eudev \
		libudev \
		udev-extraconf \
		libstdc++ \
		glib-2.0 \
		gstreamer1.0-meta-base \
		gstreamer1.0-plugins-bad \
		gstreamer1.0-plugins-base \
		gstreamer1.0-plugins-good \
		openamp-fw-echo-testd \
		openamp-fw-mat-muld \
		openamp-fw-rpc-demo \
		packagegroup-core-boot \
		packagegroup-core-ssh-dropbear \
		tcf-agent \
		watchdog-init \
		bridge-utils \
		hellopm \
		packagegroup-petalinux \
		packagegroup-petalinux-benchmarks \
		packagegroup-petalinux-gstreamer \
		packagegroup-petalinux-matchbox \
		packagegroup-petalinux-openamp \
		packagegroup-petalinux-self-hosted \
		packagegroup-petalinux-utils \
		packagegroup-petalinux-v4lutils \
		packagegroup-petalinux-x11 \
		ecam-init \
		ultra96-radio-leds \
		ultra96-wpa \
		ar1335af-mcu \
		wilc \
		libftdi \
		bonnie++ \
		cmake \
		iw \
		lmsensors-sensorsdetect \
		nano \
		packagegroup-base-extended \
		packagegroup-petalinux-96boards-sensors \
		packagegroup-petalinux-ultra96-webapp \
		python-pyserial \
		python3-pip \
		ultra96-ap-setup \
		ultra96-misc \
		wilc-firmware-wilc3000 \
		"
EXTRA_USERS_PARAMS = "usermod -P root root;"
