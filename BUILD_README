=============
Introduction:
=============
	To prepare binaries for xilinx devices, "PetaLinux" SDK setup is used. PetaLinux provides a complete, reference Linux distribution that has been integrated and tested for Xilinx devices.

===============
Pre-Requisites:
===============
	1. Host OS: Ubuntu 16:04 LTS
	2. CPU: 4 core or more recommended
	3. RAM: Minimum 8GB
	4. Storage: 30GB or more
	5. Internet Connection

=========================
Installing PetaLinux SDK:
=========================

To build the images for Xilinx Ultra96_v2 boards, PetaLinux SDK must be installed in the host Ubuntu system. Following steps are used to install the PetaLinux SDK in your system.

	1. Download the PetaLinux 2018.3 installer from the following link,
	 https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools.html

	Note: Kindly note that BSP and PetaLinux version should match, the BSP project version for e-cam project is 2018.3. So it is important to download the PetaLinux version 2018.3

	2. Run the installer,
	$ ./petalinux-v2018.3-final-installer.run <install-directory-of-choice>

	3. Accept License agreement when prompted.

	4. If the installation setup requires to change the system shell from bash to sh, then use following command to configure,
	$ sudo dpkg-reconfigure dash
 
	and answer “yes” to keep dash as /bin/sh or “no” to switch to bash.

=============================
Build Images using PetaLinux:
=============================
	
	1. Clone the BSP project from e-con gitlab.
	$ git clone https://gitlab.com/linkup1/ultra96_v2/e-cam132_cu96.git 

	2. Move to the "ultra96v2_ecam132_bsp" directory,
	$ cd e-cam132_cu96/ultra96v2_ecam132_bsp

	3. Source the settings file present inside petalinux SDK package to initialize petalinux build tools in the current terminal.
	$ source <PATH_TO_PETALINUX_SDK_DIR>/settings.sh

	4. Build the project using following command,
	$ petalinux-build

	Note: It will take few hours to complete.

	5. After the successful completion of the build, following binary files are found in the "images/linux" directory.
	zynqmp_fsbl.elf
	pmufw.elf
	system.bit
	bl31.elf
	u-boot.elf
	image.ub

	6. Create the "BOOT.bin" file using the following command,
	$ petalinux-package --boot --format BIN --fsbl images/linux/zynqmp_fsbl.elf --u-boot images/linux/u-boot.elf --pmufw images/linux/pmufw.elf --fpga images/linux/*.bit --force

==========================
SD Card Preparation Steps:
==========================

	1. Connect the SD card of size 16GB to the Ubuntu system using card reader.
	
	2. Start the partition process using "fdisk" command as follows,

	$ sudo fdisk /dev/sdX
 
	Note: X may be (a,b,c...) based on the mounting order of block device. Find the appropriated node for sdcard using dmesg command before executing the command.
	
	3. Delete the previous partitions if any using the following command,

	Command (m for help): d
	Partition number (1,2, default 2): 1
 
	Partition 1 has been deleted.
 
	Command (m for help): d
	Selected partition 2
	Partition 2 has been deleted.

	4. Create a new partition with partition type as "primary", partition number as 1 and with size 1GB.

	Command (m for help): n
	Partition type
	p   primary (0 primary, 0 extended, 4 free)
   	e   extended (container for logical partitions)
	Select (default p): p
	Partition number (1-4, default 1):
	First sector (2048-31293439, default 2048):
	Last sector, +sectors or +size{K,M,G,T,P} (2048-31293439, default 31293439): +1G
 
	Created a new partition 1 of type 'Linux' and of size 1 GiB.

	5. Make the partition 1 type as FAT32.

	Command (m for help): t
	Selected partition 1
	Partition type (type L to list all types): c
	Changed type of partition 'Linux' to 'W95 FAT32 (LBA)'.

	6. Enable the partition 1 boot flag.
	
	Command (m for help): a
	Selected partition 1
	The bootable flag on partition 1 is enabled now.

	7. Create the second partition with type as "primary", partition number as 2 with remaining size of the SD card.
	
	Command (m for help): n
	Partition type
   	p   primary (1 primary, 0 extended, 3 free)
   	e   extended (container for logical partitions)
	Select (default p): p
	Partition number (2-4, default 2):
	First sector (2099200-31293439, default 2099200):
	Last sector, +sectors or +size{K,M,G,T,P} (2099200-31293439, default 31293439):
 
	Created a new partition 2 of type 'Linux' and of size 13.9 GiB.

	8. Print the partition table.

	Command (m for help): p
	Disk /dev/sdb: 14.9 GiB, 16022241280 bytes, 31293440 sectors
	Units: sectors of 1 * 512 = 512 bytes
	Sector size (logical/physical): 512 bytes / 512 bytes
	I/O size (minimum/optimal): 512 bytes / 512 bytes
	Disklabel type: dos
	Disk identifier: 0xec8054d2
 
	Device     Boot   Start      End  Sectors  Size Id Type
	/dev/sdb1  *       2048  2099199  2097152    1G  c W95 FAT32 (LBA)
	/dev/sdb2       2099200 31293439 29194240 13.9G 83 Linux

	9. Save the partition table.
	
	Command (m for help): w
	The partition table has been altered.
	Calling ioctl() to re-read partition table.
	Syncing disks.

	10. Format the two partition in their respective file system as follows,

	a) Format boot partition using following command,

		$ sudo mkfs.vfat -n boot /dev/sdb1
 
		mkfs.fat 3.0.28 (2015-05-16)
		mkfs.fat: warning - lowercase labels might not work properly with DOS or Windows

	b) Formatting rootfs partition,

		$ sudo mkfs.ext4 -L rootfs /dev/sdb2
 
		mke2fs 1.42.13 (17-May-2015)
		/dev/sdb2 contains a ext4 file system
    		last mounted on / on Wed Jan  8 10:34:12 2020
		Proceed anyway? (y,n) y
		Creating filesystem with 3649280 4k blocks and 913920 inodes
		Filesystem UUID: f26a4cf3-441e-4b0f-a272-ce7a619d442d
		Superblock backups stored on blocks:
    		32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208
 
		Allocating group tables: done                           
		Writing inode tables: done                           
		Creating journal (32768 blocks): done
		Writing superblocks and filesystem accounting information: done
	
================================
Flash SD Card with Built Images:
================================

	1. After formatting the SD card using the procedure mentioned in the above section, disconnect and reconnect the SD card in the system.

	2. Two partition with names "boot" and "rootfs" will be shown in the system.

	3. Move to location where built binaries are present using following command,
	$ cd <PATH_TO_GIT_REPO>/ecam132_cu96/ultra96v2_ecam132_bsp/images/linux/
	
	4. Copy boot files,
	$ cp BOOT.BIN <mount-point-for-boot>
	$ cp image.ub <mount-point-for-boot>

	4. Unmount both the SD Card partitions using,
	$ umount  <mount-point-for-boot>
	$ umount  <mount-point-for-rootfs>

	5. Flash rootfs partition,
	$ sudo dd if=rootfs.ext4 of=/dev/sdX2 bs=1M
	$ sync
 
	NOTE: 
        i)  /dev/sdX2 where X may the block device (a,b,c...). Find out the correct block device corresponding to the sdcard before proceeding with flashing.
	ii) The pre-built binary file "Linkup_U96_v1_0_Bin.tar.bz2" present inside "<PATH_TO_GIT_REPO>/ecam132_cu96/pre_built_binaries" can also be used for quick testing.
		
