## ecam131_CU96 camera driver for Ultra96_V2  
Copyright © 2022 e-con Systems India Pvt. Limited All rights reserved.  

####VERSION  

LinkUP_u96_V1.0  

####FEATURES:  

e-con systems now provide camera modules for Xilinx Ultra96_V2 boards.  

Ultra96-V2 is an Arm-based, Xilinx Zynq UltraScale+ ™ MPSoC development board based on the Linaro 96Boards Consumer Edition (CE) specification.  

e-CAM130_MI1335_MOD camera module is a 13MP auto focus camera with MIPI connector designed and developed by e-con systems.    

Camera module(e-CAM130_MI1335_MOD), camera adapter board (e-CAM136_MI1335_MOD) and camera base board (ACC_96DEV_MIPICAMERA_ADP) are collectively called as e-CAM132_CU96.  

1.e-CAM130_MI1335_MOD camera driver supports following resolutions,  

	640x480  
	1280x720  
	1920x1080  
	3840x2160  

2.Image quality controls to enable adjustments  

	Brightness  
	Contrast  
	Saturation  
	Gamma  
	Gain 
	Sharpness  
	White_balance controls  
	Flip  
	Tilt and Pan  
 	Zoom  
	Exposure - Auto,Manual,Roi based  
	Exposure compensation 
	Focus, Absolute  
	Focus Auto  
	ROI Window Size  
	Focus ROI  
	ROI Exposure  
	Trigger Focus  
	iHDR  
	Denoise  
	Focus Marker  
	 

####RELEASE_CONTENTS  

	ecam132_cu96  
		-- ultra96v2_ecam132_bsp - BSP Source to build binaries for Xilinx Ultra96_V2 Board.  
		-- pre_built_binaries    - Contains pre-built binary files inside "Linkup_U96_v1_0_Bin.tar.bz2" file to quickly test the release.  
		-- BUILD_README          - Contains steps to setup the system to build images and prepare bootable SD Card.  
		-- CONTROLS_README       - Contains steps to configure media entity, stream camera and configure camera controls.  
		-- README.md             - You are reading it.  

####PREREQUISITES  

	1. Ultra96_V2 Board  
	2. e-CAM130_MI1335_MOD
	3. e-CAM136_MI1335_MOD (REV X)  
	4. ACC_96DEV_MIPICAMERA_ADP (REV X1) camera adapter developed by e-con systems.  
